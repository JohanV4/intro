import java.util.*;

public class Answer {

   public static void main (String[] param) {

      // TODO!!! Solutions to small problems 
      //   that do not need an independent method!
    
      // conversion double -> String
        String s1 = Double.toString(Math.PI); //String.valueOf(Math.PI)
      // conversion String -> int
       int s2 = Integer.parseInt("6");
      // "hh:mm:ss"

      // cos 45 deg

      // table of square roots

      String firstString = "ABcd12";
      String result = reverseCase (firstString);
      System.out.println ("\"" + firstString + "\" -> \"" + result + "\"");

      // reverse string

      String s = "How  many	 words   here";
      int nw = countWords (s);
      System.out.println (s + "\t" + String.valueOf (nw));

      // pause. COMMENT IT OUT BEFORE JUNIT-TESTING!

      final int LSIZE = 100;
      ArrayList<Integer> randList = new ArrayList<Integer> (LSIZE);
      Random generaator = new Random();
      for (int i=0; i<LSIZE; i++) {
         randList.add (Integer.valueOf (generaator.nextInt(1000)));
      }

      // minimal element

      // HashMap tasks:
      //    create
      //    print all keys
      //    remove a key
      //    print all pairs

      System.out.println ("Before reverse:  " + randList);
      reverseList (randList);
      System.out.println ("After reverse: " + randList);

      System.out.println ("Maximum: " + maximum (randList));
   }

   /** Finding the maximal element.
    * @param a Collection of Comparable elements
    * @return maximal element.
    * @throws NoSuchElementException if <code> a </code> is empty.
    */
   static public <T extends Object & Comparable<? super T>>
         T maximum (Collection<? extends T> a) 
            throws NoSuchElementException {
      return null; // TODO!!! Your code here
   }

   /** Counting the number of words. Any number of any kind of
    * whitespace symbols between words is allowed.
    * @param text text
    * @return number of words in the text
    */
   public static int countWords (String text) {
       int count = 0;
       if (!(" ".equals(text.substring(0, 1))) || !(" ".equals(text.substring(text.length() - 1))))
       {
           for (int i = 0; i < text.length(); i++)
           {
               if (text.charAt(i) == ' ')
               {
                   count++;
               }
           }
           count = count + 1;
       }
       return count;
   }

   /** Case-reverse. Upper -> lower AND lower -> upper.
    * @param s string
    * @return processed string
    */
   public static String reverseCase (String s) {
      //https://stackoverflow.com/questions/1729778/how-can-i-invert-the-case-of-a-string-in-java
      StringBuilder news = new StringBuilder();
      for (int x = 0; x < s.length(); x++)
          {
             char c = s.charAt(x);
             boolean check = Character.isUpperCase(c);
             if (check)
                news.append(Character.toLowerCase(c));
             else
                news.append(Character.toUpperCase(c));
          }
      return news.toString();
   }

   /** List reverse. Do not create a new list.
    * @param list list to reverse
    */
   public static <T extends Object> void reverseList (List<T> list)
      throws UnsupportedOperationException {
       //https://www.geeksforgeeks.org/collections-reverse-java-examples/
        Collections.reverse(list);
   }
}
